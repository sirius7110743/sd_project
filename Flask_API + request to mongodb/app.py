'''Создать методы чтения/создания/обновления/удаления API, 
коллекция должна быть для двух сущностей со связью один-ко-многим:
Чаты и сообщения'''


import json
from flask import Flask, request, jsonify
from pymongo import MongoClient

client = MongoClient(f'mongodb://mongo:mongo@localhost:37112')

db = client.movie

app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello world'


#CRUD для коллекции с чатами chat


#Для создания документа localhost:5000/chat/create
# Body зарпоса
# {
#     "name_chat": "SISTER",
#     "data_create_chat": "03.09.2023",
#     "message": [{"text": "Hello", "timesend": "2023-09-05T10:05:00Z", "user": "Denis"}, {"text": "How do you doing?", "timesend": "2023-09-05T10:07:00Z", "user": "Denis"}]
# }

@app.post('/chat/create')
def create_chat():
    body = request.json
    name_chat = body["name_chat"]
    data_create_chat = body["data_create_chat"]
    message = body["message"]

    result = db.chat.insert_one({
        "name_chat": name_chat,
        "data_create_chat": data_create_chat,
        "message": message
    })

    result.inserted_id
    return jsonify({"id": str(result.inserted_id)})



#Для чтения базы данных localhost:5000/chat/read

@app.route('/chat/read')
def chat():
    result = db.chat.find()
    message = list(result)

    return json.dumps(message, default=str), 200, {'Content-Type': 'applications/json'}




#Для обновления документа localhost:5000/chat/update
# Body зарпоса
# {
#     "name_chat": "SISTER",
#     "message": [{"text": "Hi", "timesend": "2023-09-05T10:05:00Z", "user": "Yan"}]
# }

#Апдейт с добавлением комментария в самый низ доки https://www.mongodb.com/docs/manual/reference/operator/update/set/

@app.post('/chat/update')
def update_chat():
    body = request.json
    name_chat = body["name_chat"]
    message = body["message"]

    result = db.chat.update_one({
        "name_chat": name_chat
    }, {
        "$set": {
            "message.0": message
        }
    }
    )

    if result.matched_count == 0:
        return '', 404
    else:
        return '', 204


#Для удаления документа localhost:5000/chat/delete
# Body зарпоса
# {
#     "name_chat": "SISTER"
# }

@app.delete('/chat/delete')
def delete_chat():
    body = request.json
    name_chat = body["name_chat"]

    result = db.chat.delete_one({
        "name_chat": name_chat
    })

    if result.deleted_count == 0: 
        return '', 404
    else:
        return '', 204
    











































##################################################################################

#CRUD для коллекции с сообщениями message 

# @app.post('/message/create')
# def create_message():
#     body = request.json
#     name_chat = body["name_chat"]
#     message = body["message"]

#     result = db.message.insert_one({
#         "name_chat": name_chat,
#         "message": message
#     })

#     result.inserted_id
#     return jsonify({"id": str(result.inserted_id)})


# @app.route('/message/read')
# def message():
#     result = db.message.find()
#     message = list(result)

#     return json.dumps(message, default=str), 200, {'Content-Type': 'applications/json'}


# @app.post('/message/update')
# def update_message():
#     body = request.json
#     name_chat = body["name_chat"]
#     message = body["message"]

#     result = db.message.update_one({
#         "name_chat": name_chat
#     }, {
#         "$set": {
#             "message": message
#         }
#     }
#     )

#     if result.matched_count == 0:
#         return '', 404
#     else:
#         return '', 204


# @app.delete('/message/delete')
# def delete_message():
#     body = request.json
#     name_chat = body["name_chat"]

#     result = db.message.delete_one({
#         "name_chat": name_chat
#     })

#     if result.deleted_count == 0: 
#         return '', 404
#     else:
#         return '', 204
    
