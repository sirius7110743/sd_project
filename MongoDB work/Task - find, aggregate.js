use "movie";

//1. Найти все фильмы со сценаристом Paul Feig
db.movieDetails.find({ director: 'Paul Feig' })

// 2. Найти все фильмы с жанрами Adult, History


db.movieDetails.find({ genres: { $in: ["Adult", "History"] } })

// 3. Найти все фильмы без актера Robin Shou

db.movieDetails.find({ actors: { $ne: 'Robin Shou' } })

// 4. Найти все фильмы без рейтингов TV-G, X, S

db.movieDetails.find({ rated: { $nin: ['TV-G', 'X', 'S'] } })

// 5. Найти все фильмы с условием year > 1899 and year < 1972

db.movieDetails.find({ $and: [{ year: { $gt: 1899, $lt: 1972 } }] })

// 6. Найти все фильмы с условием tomato.reviews >= 7 and tomato.reviews <= 194

db.movieDetails.find({ $and: [{ "tomato.reviews": { $gte: 7, $lte: 194 } }] })

// 7. Найти все фильмы с условием (countries = USA or (tomato.userReviews > 2541987 and tomato.userReviews < 28672969)) and (tomato.image = rotten or (tomato.userRating > 2.1 and tomato.userRating < 3.9))

db.movieDetails.find({
    $and: [
        {
            $or: [{ coutries: "USA" }, { 'tomato.userReviews': { $gt: 55, $lt: 202 } }]
        },
        {
            $or: [{ 'tomato.image': 'rotten' }, { 'tomato.userRating': { $gt: 2.1, $lt: 3.9 } }]
        }
    ]
})


// 8. Для фильмов со страной Belgium вывести топ 5 режиссеров с наибольшим количеством наград

db.movieDetails.aggregate([
    { $match: { 'countries': { $in: ['Belgium'] } } },
    { $group: { _id: '$director', sumwiners: { $sum: '$awards.wins' } } },
    { $sort: { sumwiners: -1 } },
    { $limit: 5 }
])

// 9. Для фильмов с tomato изображением fresh вывести топ 9 актеров с наибольшим средним imdb рейтингом

db.movieDetails.aggregate([
    { $unwind: "$actors" },
    { $match: { 'tomato.image': 'fresh' } },
    { $group: { _id: '$actors', movies: { $push: '$$ROOT' }, imdb: { $avg: "$imdb.rating" } } },
    { $sort: { imdb: -1 } },
    { $limit: 9 }
])

