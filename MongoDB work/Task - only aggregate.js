use "movie"

//Для фильмов с рейтингом R вывести топ 5 актеров с наибольшим количеством фильмов.

db.movieDetails.aggregate([
    {$unwind: "$actors"},
    { $match: { actors: { $ne: null }, rated: 'R' } },
    { $group: {_id: '$actors', movies: { $push: '$$ROOT' }, sumMovie: { $sum: 1}} },
    { $sort: { sumMovie: -1 } },
    { $limit: 5 }
])


//Для фильмов выпущенных с 2000 по 2010 годы вывести топ 10 режисеров с наибольшим средним tomato рейтингом.

db.movieDetails.aggregate([
    {$match: {'director': { $ne: null}}}, 
    {$match: {$and: [{year: {$gte: 2000, $lte: 2010}}]}},
    {$group: { _id: "$director", movies: {$push: '$$ROOT'}, tomato_rating: {$avg: "$tomato.rating"}}},
    {$sort:{tomato_rating: -1}},
    {$limit:10}
])


//Для фильмов из Италии и Франции вывести топ 3 сценариста с наибольшим количеством наград.

db.movieDetails.aggregate([
    {$unwind: "$writers"},
    {$match:{ 'countries': {$in: ['Italy', 'France']}}},
    {$group:{_id:'$writers', sumwiners:{$sum: '$awards.wins'}}},
    {$sort: {sumwiners: -1}},
    {$limit:3}
])



//Для фильмов с metacritic > 70 вывести топ 3 жанра с наибольшим количеством наград.

db.movieDetails.aggregate([
    {$unwind: "$genres"},
    {$match: {metacritic: {$gt: 70}}},
    {$group: {_id: '$genres', awards_wins: {$sum: '$awards.wins'}}},
    {$sort: {awards_wins: -1}},
    {$limit:3}
])

db.movieDetails.aggregate([
    {$unwind: "$genres"},
])

