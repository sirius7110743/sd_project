use Test_1_30_10_23;

//ЗАДАЧА 1

//  (2 коллекции). Репозитории и тикеты:

// репозиторий может иметь несколько тикетов
// тикет может принадлежать только одному репозиторию
// репозиторий - название, описание, количество звезд
// тикет - название, описание, статус

// Сделать вывод связанных сущностей за один запрос.

db.repositories.drop()
db.tiket.drop()

db.createCollection('repositories', {validator:{ $jsonSchema: {
        bsonType: 'object',
        properties: {
            _id: {bsonType: 'number'},
            name: {bsonType: 'string'},
            discription: {bsonType: 'string'},
            count_stars: {bsonType: 'number'},
            tiket_ids: {bsonType: 'array', items: { bsonType: 'number'}}
        },
        required: ['_id', 'name'],
        additionalProperties: false
}}})

db.createCollection('tiket', {validator:{ $jsonSchema: {
        bsonType: 'object',
        properties: {
            _id: {bsonType: 'number'},
            name: {bsonType: 'string'},
            discription: {bsonType: 'string'},
            status: {bsonType: 'string'}
        },
        required: ['_id', 'name'],
        additionalProperties: false
}}})


db.repositories.insertMany([{_id: 1, "name": 'MongoDB_rep', discription: "Репозиорий для баз данных", count_stars: 2, tiket_ids: [1, 2]},
{_id: 2, "name": 'Programmer_rep', discription: "Репа для прогеров", count_stars: 4, tiket_ids: [3]},
{_id: 3, "name": 'Sysadmin_rep', discription: "Репа для сисадминов", count_stars: 6, tiket_ids: [4]}
])

db.tiket.insertMany([{_id: 1, "name": 'MongoDB_tiket', discription: "Тикет для баз данных", status: "В проде"},
{_id: 2, "name": 'Programmer_tiket', discription: "Тикет для прогеров", status: "В проде"},
{_id: 3, "name": 'Sysadmin_tiket', discription: "Тикет для сисадминов", status: "В проде"},
{_id: 4, "name": 'Mongo_tiket_1', discription: "Тикет для сисадминов", status: "В проде"}
])


db.repositories.aggregate({
    $lookup: {
          from: "tiket",
          localField: "tiket_ids",
          foreignField: "_id",
          as: "task"
         }
})




//ЗАДАЧА 2

// 2. (1 коллекция) Статьи и комментарии:

// статья может иметь несколько комментариев
// комментарий может принадлежать только одной статьемассивы
// статья - название, текст, дата публикации
// комментарий - текст, количество лайков

db.article.drop()

db.createCollection('article', {validator:{ $jsonSchema: {
        bsonType: 'object',
        properties: {
            _id: {bsonType: 'number'},
            name: {bsonType: 'string'},
            date_publish: {bsonType: 'date'},
            comments: {
                    bsonType: 'array',
                    items: {
                        bsonType: 'object',
                        properties: {
                            _id: {bsonType: 'number'},
                            text: { bsonType: 'string'},
                            count_likes: { bsonType: 'number'}
                        }
                    }
                }
        },
        required: ['name'],
        additionalProperties: false
    }
}})



db.article.insertMany([
    {_id: 1, name: 'Denis', date_publish: new Date ('2023-08-19'), comments: [{_id: 1, text: "Коментарий для статьи", count_likes: 2}, {_id: 2, text: "Коментарий для статьи", count_likes: 30}]},
    {_id: 2, name: 'Gosha', date_publish: new Date ('2023-07-19'), comments: [{_id: 3, text: "Коментарий для статьи", count_likes: 40}, {_id: 3, text: "Коментарий для статьи", count_likes: 6}]},
    {_id: 3, name: 'Ivan', date_publish: new Date ('2023-07-20'), comments: [{_id: 4, text: "Коментарий для статьи", count_likes: 7}, {_id: 5, text: "Коментарий для статьи", count_likes: 70}]},
    {_id: 4, name: 'Ismail', date_publish: new Date ('2023-09-20'), comments: [{_id: 6, text: "Коментарий для статьи", count_likes: 18}, {_id: 7, text: "Коментарий для статьи", count_likes: 40}]},
])

db.article.find()



//ЗАДАЧА 3

// 3. (2 коллекции) Музыка и музыканты:

// у музыканта может быть много музыкальных произведений
// авторами музыкальных произведений могут быть несколько музыкантов
// музыкальное произведение - название, жанр, длительность
// музыкант - имя, фамилия, год рождения

// Написать 2 запроса на вывод связанных сущностей - оба возможных варианта.

db.music.drop()
db.authors.drop()


db.createCollection('music', {validator:{ $jsonSchema: {
        bsonType: 'object',
        properties: {
            _id: {bsonType: 'number'},
            name: {bsonType: 'string'},
            janre: {bsonType: 'string'},
            time: {bsonType: 'number'},
            authors_ids: {
                    bsonType: 'array', items: { bsonType: "number" }
                }
        },
        required: ['name'],
        additionalProperties: false
    }
}})


db.createCollection('authors', {validator:{ $jsonSchema: {
        bsonType: 'object',
        properties: {
            _id: {bsonType: 'number'},
            name: {bsonType: 'string'},
            data_born: {bsonType: 'date'}
        },
        required: ['name'],
        additionalProperties: false
    }
}})



db.music.insertMany([
    {_id: 1, name: 'Music_1', janre: 'drama', time: 1, authors_ids: [2, 3]}, 
    {_id: 2, name: 'Music_2', janre: 'drama', time: 1, authors_ids: [2, 3, 4]}, 
    {_id: 3, name: 'Music_3', janre: 'drama', time: 1, authors_ids: [1, 2, 3, 4]},
    {_id: 4, name: 'Music_4', janre: 'drama', time: 1, authors_ids: [2, 3, 4]}
])

db.authors.insertMany([
    {_id: 1, name: 'Kathryn Stockett'},
    {_id: 2, name: 'Stephen Edwin King'},
    {_id: 3, name: 'J. K. Rowling'},
    {_id: 4, name: 'Harriet Beecher Stowe'}
])

db.authors.aggregate({
    $lookup: {
          from: "music",
          localField: "_id",
          foreignField: "authors_ids",
          as: "music"
         }
})


db.music.aggregate({
    $lookup: {
          from: "authors",
          localField: "authors_ids",
          foreignField: "_id",
          as: "authors"
         }
})
