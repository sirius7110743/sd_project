import logging
import telebot
from telebot import types
from pymongo import MongoClient
import os

logging.basicConfig(level=logging.DEBUG)

MONGO_HOST = os.getenv('MONGO_HOST')
MONGO_PORT = os.getenv('MONGO_PORT')
MONGO_USER = os.getenv('MONGO_USER')
MONGO_PASSWORD = os.getenv('MONGO_PASSWORD')
MONGO_DB = os.getenv('MONGO_DB')
TELEBOT_TOKEN = os.getenv('TELEBOT_TOKEN')


client = MongoClient(f'mongodb://{MONGO_USER}:{MONGO_PASSWORD}@{MONGO_HOST}:{MONGO_PORT}')
#client = MongoClient(f'mongodb://mongo:mongo@localhost:37112') #подключение к бд
db = client.get_database('Bot_Carwash') # выбор бд в которой будет запись производиться

client_collection = 'client_collection'
chats_collection = 'chats'


bot = telebot.TeleBot('6754744043:AAG4yMMwYizx-V66223SRF0DHNxjGPR9oeA') #HTTP API


start_menu = [
    'Записаться на мойку',
    'Узнать время записи на мойку',
    'Изменить время записи на мойку',
    'Отменить запись на мойку']


@bot.message_handler(commands=['start'])
def start(message):
    try:
        chat = db[chats_collection].find_one({'_id': message.chat.id})
        if chat is None:
            db[chats_collection].insert_one(
                {'_id': message.chat.id})
            
        bot.send_message(message.chat.id, '''Вас приветствует телеграмм-бот автомобильной мойки на Федеральной территории "Сириус"\n"Sirius_Carwash""''')

        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        buttons = []

        for menu in start_menu:
            buttons.append(types.KeyboardButton(
                menu))

        markup.add(*buttons)

        bot.send_message(message.chat.id, '''Выберите желаемую услугу''', reply_markup=markup)

    except Exception as e:
        print(repr(e))



@bot.message_handler(content_types=["text"])
def Hairdresser(message):
    if message.text == "Записаться на мойку":
        customer_records_message=[]
        @bot.message_handler(content_types=["text"])
        def FIO_function(message):
            customer_records_message.append(message.text)
            bot.send_message(message.chat.id, '''Введите ваши Фамилию и Имя''')
            bot.register_next_step_handler(msg, Tel_function)
            print(customer_records_message)

        msg = bot.send_message(message.chat.id, '''Вы выбрали услугу записаться на мойку\nВведите номер телефона для связи''')
        bot.register_next_step_handler(msg, FIO_function)

        @bot.message_handler(content_types=["text"])
        def Tel_function(message):
            customer_records_message.append(message.text)
            msg = bot.send_message(message.chat.id, '''Введите время на которое вы бы хотели записаться''')
            bot.register_next_step_handler(msg, Time_function)
            print(customer_records_message)

        
        @bot.message_handler(content_types=["text"])
        def Time_function(message):
            customer_records_message.append(message.text)
            result = db.client_collection.insert_one({
            "Tel_number": customer_records_message[0],
            "FIO_clients": customer_records_message[1],
            "Time_records": customer_records_message[2]})
            print(customer_records_message)
            msg = bot.send_message(message.chat.id, '''Вы успешно записались на мойку''')

    if message.text == "Узнать время записи на мойку":

        msg = bot.send_message(message.chat.id, '''Чтобы узнать время записи на мойку\nВведите номер телефона, указанный в форме при записи''')

        customer_records_message=[]

        @bot.message_handler(content_types=["text"])
        def Return_time_function(message):
            customer_recorder = db[client_collection].find_one({'Tel_number': message.text})
            if customer_recorder==None:
                bot.send_message(message.chat.id, '''Такая запись не существует''')
                msg = bot.send_message(message.chat.id, '''Попробуйте еще раз''')
                bot.register_next_step_handler(msg, Return_time_function)
            else:
                customer_records_message.append(message.text)
                print(customer_records_message)
                msg = bot.send_message(message.chat.id, '''Такая запись существует''')
                carwash = db[client_collection].find_one({"Tel_number": customer_records_message[0]})
                result = f'''
        Телефон: {carwash["Tel_number"]}, 
        ФИО: {carwash["FIO_clients"]}, 
        Время записи: {carwash["Time_records"]}'''
                bot.send_message(message.chat.id, result)

        bot.register_next_step_handler(msg, Return_time_function)




    if message.text == "Изменить время записи на мойку":
        customer_records_message=[]
        msg = bot.send_message(message.chat.id, '''Чтобы изменить время мойки\nВведите номер телефона, указанный в форме при записи''')

        @bot.message_handler(content_types=["text"])
        def Return_time_function(message):
            customer_recorder = db[client_collection].find_one({'Tel_number': message.text})
            if customer_recorder==None:
                bot.send_message(message.chat.id, '''Такая запись не существует''')
                msg = bot.send_message(message.chat.id, '''Попробуйте еще раз''')
                bot.register_next_step_handler(msg, Return_time_function)
            else:
                customer_records_message.append(message.text)
                msg = bot.send_message(message.chat.id, '''Введите время на которое хотели бы перезаписаться''')
                bot.register_next_step_handler(msg, Time_function)

        bot.register_next_step_handler(msg, Return_time_function)
        
        @bot.message_handler(content_types=["text"])
        def Time_function(message):
            result = db[client_collection].update_one({
            "Tel_number": customer_records_message[0]
        }, {
            "$set": {
                "Time_records": message.text
            }
        }
        )
        
            carwash = db[client_collection].find_one({"Tel_number": customer_records_message[0]})
            msg = bot.send_message(message.chat.id, '''Перезапись прошла успешно, ждем вас''')
            result = f'''
    Телефон: {carwash["Tel_number"]}, 
    ФИО: {carwash["FIO_clients"]}, 
    Время записи: {carwash["Time_records"]}'''
            bot.send_message(message.chat.id, result)


    if message.text == "Отменить запись на мойку":
        customer_records_message=[]
        msg = bot.send_message(message.chat.id, '''Чтобы отменить запись на мойку\nВведите номер телефона, указанный в форме при записи''')
        @bot.message_handler(content_types=["text"])
        def Stop_record_function(message):
            customer_recorder = db[client_collection].find_one({'Tel_number': message.text})
            if customer_recorder==None:
                bot.send_message(message.chat.id, '''Такая запись не существует''')
                msg = bot.send_message(message.chat.id, '''Попробуйте еще раз''')
                bot.register_next_step_handler(msg, Stop_record_function)
            else:
                msg = bot.send_message(message.chat.id, '''Такая запись существует, вы точно хотите отменить запись?''')
                customer_records_message.append(message.text)
                bot.register_next_step_handler(msg, confirmation_function)

        bot.register_next_step_handler(msg, Stop_record_function)

        @bot.message_handler(content_types=["text"])
        def confirmation_function(message):
                try:

                    if message.text=='да':
                        result = db[client_collection].delete_one({
                        "Tel_number": customer_records_message[0]
                        })
                        msg = bot.send_message(message.chat.id, '''Удаление записи прошло успешно''')
                    elif message.text=='нет':
                        msg = bot.send_message(message.chat.id, '''Удаление записи отмененно''')
                    else:
                        msg = bot.send_message(message.chat.id, '''Ошибка! Введите еще раз)''')
                        bot.register_next_step_handler(msg, confirmation_function)
                except:
                    bot.send_message(message.chat.id, '''Ошибка!''')

                


logging.debug('Bot started.')

bot.polling()