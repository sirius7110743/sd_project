Flask==2.3.3
pymongo==4.6.0
redis==5.0.1
prometheus-flask-exporter==0.23.0
python-dotenv==1.0.0
python-logging-loki==0.3.1
urllib3==1.26.18