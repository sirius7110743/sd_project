import json
from flask import Flask, request, jsonify
from pymongo import MongoClient
import os
from prometheus_flask_exporter import PrometheusMetrics
from prometheus_client import Counter
from redis import Redis
import logging
from logging_loki import LokiHandler, emitter
from dotenv import load_dotenv
from migration import migrate
load_dotenv()

LOKI_HOST = os.getenv('LOKI_HOST') or 'localhost'
LOKI_PORT = os.getenv('LOKI_PORT')

emitter.LokiEmitter.level_tag = "level"
loki_handler = LokiHandler(
    url=f"http://{LOKI_HOST}:{LOKI_PORT}/loki/api/v1/push",
    tags={"job": "python-flask"},
    version="1"
)

logger = logging.getLogger('logger')
logger.setLevel(logging.DEBUG)
logger.addHandler(loki_handler)

# todo: Flask 3.0.0

MONGO_HOST = os.getenv('MONGO_HOST') or 'localhost'
MONGO_PORT = os.getenv('MONGO_PORT')
MONGO_USER = os.getenv('MONGO_USER')
MONGO_PASSWORD = os.getenv('MONGO_PASSWORD')
MONGO_DB = os.getenv('MONGO_DB')

REDIS_HOST = os.getenv('REDIS_HOST') or 'localhost'
REDIS_PORT = os.getenv('REDIS_PORT')

client = MongoClient(
    f'mongodb://{MONGO_USER}:{MONGO_PASSWORD}@{MONGO_HOST}:{MONGO_PORT}')
db = client.get_database(MONGO_DB)

migrate(db, message='message')
redis_client = Redis(host=REDIS_HOST, port=REDIS_PORT, decode_responses=True)


app = Flask(__name__)
metrics = PrometheusMetrics(app)
app.config['JSON_AS_ASCII'] = False

name_chat_counter = Counter(
    'chat_requests_counter',
    'Requests of chat by genre',
    ['name_chat']
)


@app.get('/')
def hello():
    return 'Hello Flask!'

# Через url
# @app.get('/message')
# def movies():
#     name_chat = request.args.get('name_chat')

#     name_chat_counter.labels(name_chat=name_chat).inc()

#     key_prefix = 'message?name_chat='
#     message = redis_client.get(key_prefix + name_chat)
#     if message:
#         logger.debug('cache hit')
#         return message, 200, {'Content-Type': 'application/json'}

#     logger.debug('cache missed')
#     result = db.Chat.find({"name_chat": name_chat})
#     message = list(result)

#     redis_client.set(key_prefix + name_chat,
#                      json.dumps(message, default=str), ex=10)

#     return json.dumps(message, default=str), 200, {'Content-Type': 'application/json'}

# Через postman
@app.get('/message')
def games():
    name_chat = request.json["name_chat"]
    name_chat_counter.labels(name_chat=name_chat).inc()

    key_prefix = 'message?name_chat='
    games = redis_client.get(key_prefix + name_chat)
    if games:
        logger.debug('cache hit')
        return games, 200, {'Content-Type': 'application/json'}

    logger.debug('cache missed')
    pipeline = [{"$match": {"name_chat": name_chat}},
                {"$sort": {"_id": -1}}, {"$limit": 10}]

    result = db.message.aggregate(pipeline)
    message = list(result)

    redis_client.set(key_prefix + name_chat,
                     json.dumps(message, default=str), ex=10)

    return json.dumps(message, default=str), 200, {'Content-Type': 'application/json'}




# @app.get('/movies')
# def movies():
#     body = request.json
#     id = body["_id"]
#     name_chat = body["name_chat"]

#     # genre = request.args.get('genre')

#     # genre_counter.labels(genre=genre).inc()

#     # key_prefix = 'movies?genre='
#     message = redis_client.get(str(id) + name_chat)
#     if message:
#         logger.debug('cache hit')
#         return message, 200, {'Content-Type': 'application/json'}

#     logger.debug('cache missed')
#     result = db.Chat.find({"name_chat": name_chat})
#     message = list(result)

#     redis_client.set(id + name_chat,
#                      json.dumps(message, default=str), ex=10)

#     return json.dumps(message, default=str), 200, {'Content-Type': 'application/json'}


@app.post('/chat/create')
def create_chat():
    body = request.json
    name_chat = body["name_chat"]
    data_create_chat = body["data_create_chat"]
    message = body["message"]

    result = db.chat.insert_one({
        "name_chat": name_chat,
        "data_create_chat": data_create_chat,
        "message": message
    })

    result.inserted_id
    return jsonify({"id": str(result.inserted_id)})


@app.post('/chat/update')
def update_chat():
    body = request.json
    name_chat = body["name_chat"]
    message = body["message"]

    result = db.chat.update_one({
        "name_chat": name_chat
    }, {
        "$set": {
            "message.0": message
        }
    }
    )

    if result.matched_count == 0:
        return '', 404
    else:
        return '', 204


@app.delete('/chat/delete')
def delete_chat():
    body = request.json
    name_chat = body["name_chat"]

    result = db.chat.delete_one({
        "name_chat": name_chat
    })

    if result.deleted_count == 0: 
        return '', 404
    else:
        return '', 204

































