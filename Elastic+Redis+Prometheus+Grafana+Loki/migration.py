import json

def migrate(db, message):
    db[message].drop()

    with open("message.json") as f:
        messageData = json.load(f)
        db[message].insert_many(messageData)