from flask import Flask, request, jsonify, send_file
from elasticsearch import Elasticsearch

app = Flask(__name__)
app.json.ensure_ascii = False


es_connection = Elasticsearch('http://localhost:41554')


@app.get('/')
def autocomplete_page():
    return send_file('static/autocomplete.html')


@app.get('/fuzzy_search')
def fuzzy_search():
    body = request.json
    message = body["message"]

    query = {
        "match": {
            "message": {
                "query": message,
                "fuzziness": 2
            }
        }
    }

    es_result = es_connection.search(index='messages', query=query)
    hits = es_result['hits']['hits']

    result = []

    for hit in hits:
        result.append(hit['_source']['message'])

    return jsonify(result)



@app.get('/morphology_search')
def morphology_search():
    body = request.json
    parent = body["parent"]

    query = {
        "match": {
            "parent": {
                "query": parent
            }
        }
    }

    es_result = es_connection.search(index='parentes', query=query)
    hits = es_result['hits']['hits']

    result = []

    for hit in hits:
        result.append(hit['_source']['parent'])

    return jsonify(result)


@app.get('/autocomplete')
def autocomplete():
    word = request.args.get('word', '')

    query = {
        "match": {
            "parent": {
                "query": word
            }
        }
    }

    es_result = es_connection.search(index='parents', query=query)
    hits = es_result['hits']['hits']

    result = []

    for hit in hits:
        result.append(hit['_source']['parent'])

    return jsonify(result)
    

